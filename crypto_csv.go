package main

import (
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"crypto_csv/coinbasepro"
)

const (
	apiFilename = "api.json"
	productID   = "BTC-GBP"
)

var maxOrdersLookup int

func main() {
	flag.IntVar(&maxOrdersLookup, "num", 100, "")
	flag.Parse()

	apiKey := loadAPIKey(apiFilename)
	client := coinbasepro.NewClient(apiKey)

	filledOrders := loadFilledOrders(client, maxOrdersLookup)
	if len(filledOrders) <= 0 {
		fmt.Println("no orders found")
		return
	}
	writeCSV(filledOrders)
	fmt.Printf("%v records written\n", len(filledOrders))
}

func loadAPIKey(filename string) coinbasepro.APIKey {
	var ret coinbasepro.APIKey

	b, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(fmt.Errorf("can't find %v", filename))
	}
	err = json.Unmarshal(b, &ret)
	if err != nil {
		panic(err)
	}
	return ret
}

func loadFilledOrders(client *coinbasepro.Client, maxCount int) []coinbasepro.Fill {
	var fills []coinbasepro.Fill

	params := coinbasepro.ListFillsParams{
		ProductID: productID,
	}
	cursor := client.ListFills(params)

	for cursor.HasMore {
		var tmp []coinbasepro.Fill
		if err := cursor.NextPage(&tmp); err != nil {
			panic(err)
		}
		fills = append(fills, tmp...)

		if len(fills) >= maxCount {
			return fills[:maxCount]
		}
	}
	return fills
}

func writeCSV(filledOrders []coinbasepro.Fill) {
	f, e := os.Create("./orders.csv")
	if e != nil {
		fmt.Println(e)
	}

	data := make([][]string, len(filledOrders)+1)
	data[0] = fillHeaders()

	if len(data[0]) != len(fillValues(filledOrders[0])) {
		panic("header/value number mismatch")
	}

	writer := csv.NewWriter(f)

	for i, order := range filledOrders {
		data[i+1] = fillValues(order)
	}

	e = writer.WriteAll(data)
	if e != nil {
		fmt.Println(e)
	}
}

func fillHeaders() []string {
	return []string{
		"TradeID",
		"FillID",
		"Side",
		"Price",
		"Size",
		"Fee",
		"CreatedAt",
	}
}

func fillValues(o coinbasepro.Fill) []string {
	return []string{
		strconv.Itoa(o.TradeID),
		o.FillID,
		o.Side,
		o.Price,
		o.Size,
		o.Fee,
		o.CreatedAt.Time().Format("2006-01-02 15:04:05"),
	}
}
