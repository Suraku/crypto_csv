# README #

Export CSV file of your chosen Coinbase Portfolio's recent orders.

## Checksum ##
SHA256: E0459373443C157186DE6D0AC06C2DCEAD0AC9B2C6959DF005CEA6440976D3F2

### Input params ###
* Default number records is 100 (based on per cursor pagination returned from Coinbase API)
* Override can be done by adding flag "num" in Console window (see Execution below)

### Setup ###

* Download this repo, or just the crypto_csv.exe and api.json file
* Generate API key on CoinbasePro
* NOTE 1: View only permission - no others required (and for safety, recommend not adding other permissions)
* NOTE 2: Make sure note down the passphrase and secret as they appear, throughout the creation process
* Fill in the blanks in api.json file with the Coinbase key information, by editing in Notepad or your preferred text editor.
* Execute :)

### Execution ###

* Run the exe file through Windows FileExplorer. Make sure your api.json file has been completed. If any errors, try the below options as these will provide error information.
* With CmdLine/Bash, navigate to the same directory as the .exe and .json file
* Bash: ./crypto_csv.exe -num 25
* CmdLine: crypto_csv.exe -num 25

### Troubleshooting ###
When in console window:
* "is not recognized as an internal or external command, operable program or batch file." - is your current console working directory the same as where your .exe and .json file is located on the file system?
* "Missing client environment variables" - your api.json file needs filling in, ensure all 3 values are completed.

### Build from Src ###

* Install Go
* In command line: go build
* No other dependencies are required.

### 3rd Party ###

* Coinbasepro package files taken from https://github.com/preichenberger/go-coinbasepro
